import javax.script.ScriptEngine; // Deprecated soon JDK11
import javax.script.ScriptEngineManager; // Deprecated soon JDK11
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;

public class Semantico {

    private List<Variable> Variables = new ArrayList<>();
    private String[] entrada;

    public Semantico(String entrada) {
        this.entrada = entrada.split(";");  // Divide las lineas de la entrada
    }

    public void analizar() {
        for (String linea : entrada) {
            if (linea.trim().startsWith("int")) {
                // Declaracion de variables tipo int
                String[] var = linea.trim().substring(4).split(",");
                Declaracion("int", var); // Agrega las variables declaradas a una lista
            } else if (linea.trim().startsWith("double")) {
                // Declaracion de variables tipo double
                String[] var = linea.trim().substring(7).split(",");
                Declaracion("double", var);
            } else if (linea.contains("=")) {
                /*
                Aqui tengo dos casos, uno en el cual la variable ya ha sido declarada
                y se le asigna una expresion (ej. c = n1 + n2 * n3;)
                Se declara una nueva variable y se le asigna una operacion (ej. int a = 34 + n2;
                 */
            }
        }

        Variables.forEach(e -> {
            System.out.println(e.toString());
        });
    }

    /**
     * @param tipo     El tipo de variable de la(s) variable(s) declaradas
     * @param variable La(s) variable(s) declaradas
     **/
    private void Declaracion(String tipo, String[] variable) {
//        int a = 23.4;
        int b = 3, c;
//        c = a + b;

        for (String var : variable) {
            boolean isOperacion = false;
            if (var.contains("=")) {
                // Si a la variable declarada se le asigna un valor
                String[] s = var.split("=");
                String asignacion = s[1].trim();
                switch (tipo) {
                    case "int":
                        try { // ^([-+]? ?([A-Za-z0-9 ]+|\((?0)\))( ?[-+*/] ?\g<1>)?)$ // Recursividad no soportada por IntelliJ
                            if (asignacion.matches("([-+]?[A-Za-z0-9 ]*\\.?[A-Za-z0-9 ]+[/+\\-*])+([-+]?[A-Za-z0-9 ]*\\.?[A-Za-z0-9 ]+)")) {
                                isOperacion = true;
                                int res = (int) Double.parseDouble(Operacion("int", asignacion).toString());
                                Variables.add(new Variable(tipo, s[0].trim(),
                                        String.valueOf(res)));
                            } else {
                                Integer.parseInt(asignacion);
                            }
                        } catch (NumberFormatException ex) { // La asignacion es una expresion
                            System.out.println("Error: No se puede asignar este valor a un tipo int");
                            System.exit(1);
                        }
                        break;
                    case "double":
                        try {
                            Double.parseDouble(asignacion);
                        } catch (NumberFormatException ex) {
                            System.out.println("Error: Variable incompatible ");
                            System.exit(1);
                        }
                        break;
                }
                if (!isOperacion) Variables.add(new Variable(tipo, s[0].trim(), s[1].trim()));
            } else {
                Variables.add(new Variable(tipo, var.trim()));
            }
        }
    }

    /**
     * @param tipo      tipo de variable a operar
     * @param operacion la operacion a realizar
     * @return el resultado, None si no se pudo operar
     **/
    private Object Operacion(String tipo, String operacion) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");

        String[] ops = operacion.split("[+|=|\\-|*|/]"); // Array con todos los operandos


        for (String op : ops) { //Recorro el arrays de variables
            String var = op.trim();
            Variables.forEach(e -> {
                if (e.getVar().equals(var)) {
                    switch (tipo) {
                        case "int":
                            try {
                                engine.put(e.getVar(), Integer.parseInt(e.getValor()));
                            } catch (NumberFormatException ex) {
                                System.out.println("Error: No se puede asignar este valor a un tipo int");
                                System.exit(1);
                            }
                            break;
                    }
                }
            });
        }

        try {
            return engine.eval(operacion);
        } catch (ScriptException e) {
            System.out.println("Error " + e.getMessage());
            return "None";
        }
    }
}
