public class Variable {
    private String tipo;
    private String var;
    private String valor;

    public Variable() {
        this("None", "None", "None");
    }

    public Variable(String tipo, String var) {
        this(tipo, var, "None");
    }

    public Variable(String tipo, String var, String valor) {
        this.tipo = tipo;
        this.var = var;
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Variable{" +
                "tipo='" + tipo + '\'' +
                ", var='" + var + '\'' +
                ", valor='" + valor + '\'' +
                '}';
    }
}
